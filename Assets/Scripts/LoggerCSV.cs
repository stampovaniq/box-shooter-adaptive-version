﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class LoggerCSV : MonoBehaviour
{
    [SerializeField] private String logDirectory;
    protected readonly object lockObj = new object();

    private string currentDate = DateTime.Now.ToString("yyyyMMdd");
    private string logCommonFileName = "log";
    private string serviceFileName = "LoggingService";
    private string prefixFileName = "Adaptive";

    private string columnNamesCommonLog = "time; result; gameTime; level";
    private StreamWriter streamWriter;

    public void Log(String logMessage)
    {
        //String currentDate = DateTime.Now.ToString("yyyyMMdd");
        //String logFileName = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + logDirectory +
        //                     Path.DirectorySeparatorChar + "log" + currentDate + ".csv";
        //lock (lockObj)
        //{
        //    if (
        //        (logFileName.Contains("/") &&
        //         Directory.Exists(logFileName.Substring(0, logFileName.LastIndexOf('/')))) ||
        //        (logFileName.Contains("\\") &&
        //         Directory.Exists(logFileName.Substring(0, logFileName.LastIndexOf('\\'))))
        //    )
        //    {
        //        bool doesFileExist = File.Exists(logFileName);
        //        using (StreamWriter streamWriter = File.AppendText(logFileName))
        //        {
        //            if (!doesFileExist)
        //            {
        //                streamWriter.WriteLine("time; result; gameTime; level");
        //            }
        //            streamWriter.WriteLine("{0}; {1}", DateTime.Now.ToString("hh:mm:ss:ffff"), logMessage);
        //            streamWriter.Close();
        //        }
        //    }
        //}

        lock (lockObj)
        {
            LogMessageInCSV(logMessage, columnNamesCommonLog, logCommonFileName);
        }
    }

    private void LogMessageInCSV(string logMessage, string columnNames, string fileName)
    {
        //FileStream fileStream;
        //String logFileName = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + logDirectory + Path.DirectorySeparatorChar + PlayerPrefs.GetString("email") + "_" + fileName + currentDate + ".csv";
        String fileNameToUpload = prefixFileName + "_" + PlayerPrefs.GetString("email") + "_" + fileName + currentDate + ".csv";
        //String logFileName = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + logDirectory + Path.DirectorySeparatorChar + serviceFileName + ".php";
        //String logFileName = "http:" + Path.DirectorySeparatorChar + Path.DirectorySeparatorChar + "www.huibg.000webhostapp.com" + Path.DirectorySeparatorChar + serviceFileName + ".php";
        String logFileName = "http://huibg.000webhostapp.com/" + serviceFileName + ".php";
        //Debug.Log(logFileName);
        //if (Application.platform == RuntimePlatform.WebGLPlayer)
        //{
        //try
        //{
        StartCoroutine(logWebServiceCall(logFileName, fileNameToUpload, columnNames, DateTime.Now.ToString("hh:mm:ss:ffff") + "," + logMessage));
        //}
        //catch (Exception e)
        //{
        //    Debug.Log(e.Message);
        //}

        //}
        //else
        //{
        //if ((logFileName.Contains("/") && Directory.Exists(logFileName.Substring(0, logFileName.LastIndexOf('/'))))
        //    || (logFileName.Contains("\\") && Directory.Exists(logFileName.Substring(0, logFileName.LastIndexOf('\\')))))
        //{
        //    try
        //    {
        //        bool doesFileExist = File.Exists(logFileName);
        //        if (doesFileExist)
        //        {
        //            fileStream = File.Open(logFileName, FileMode.Append);
        //            streamWriter = new StreamWriter(fileStream);
        //            logMessage = logMessage.Replace(".", ",");
        //            streamWriter.WriteLine("{0}; {1}", DateTime.Now.ToString("hh:mm:ss:ffff"), logMessage);
        //            streamWriter.Close();
        //        }
        //        else
        //        {
        //            fileStream = File.Create(logFileName);
        //            streamWriter = new StreamWriter(fileStream);
        //            streamWriter.WriteLine(columnNames);
        //            streamWriter.Close();
        //        }

        //        fileStream.Close();

        //        if (Application.platform == RuntimePlatform.WebGLPlayer)
        //        {
        //            Application.ExternalEval("SyncFiles");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        PlatformSafeMessage("Failed to Load: " + e.Message);
        //    }
        //}
        //}
    }

    IEnumerator logWebServiceCall(string logFileName, string fileName, string columnNames, string logContent)
    {
        //http://127.0.0.1/KitchenAR/test.php 
        WWW w = new WWW(logFileName + "?filename=" + fileName + "&&columnNames=" + columnNames + "&&logContent=" + logContent);
        /*while (!w.isDone)
        {
            //Debug.Log("downloading...");
        }*/
        yield return w;
    }
}
