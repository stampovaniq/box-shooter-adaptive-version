﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class DificultyChanger
{
    #region Properties

    #region For Calculating Score Result

    public int Score1;
    public int Score2;
    public float TimeForScore1;
    public float TimeForScore2;

    #endregion For Calculating Score Result

    #region For Calculating Time Result

    public float CurrentTime;
    public float TimeToCompare;

    #endregion For Calculating Time Result

    #region For Calculating Efficiency

    public double CountHits;
    public double CountFiredShots;

    #endregion For Calculating Efficiency

    #region Results

    public string ScoreResult;
    public string TimeResult;

    #endregion Results

    #region Modifiers

    public float SpeedModifier;
    public float FogModifier;
    public float RainModifier; //TODO check maybe its not float

    #endregion Modifiers

    private const float ControlScoreForScoreResult = 1.25f;
    private const float DividerTwo = 2f;
    private const string Low = "low";
    private const string High = "high";

    #endregion Properties

    #region Methods

    /// <summary>
    /// Use this with initialized objects
    /// </summary>
    /// <returns>Based On two taken scores at at given time return if the score is low or high</returns>
    public void CalculateScoreResult()
    {
        var result = (this.Score2 - this.Score1) / (DividerTwo * (this.TimeForScore2 - this.TimeForScore1));

        //Debug.Log("Score Result:" + result);

        if (result < ControlScoreForScoreResult)
        {
            this.ScoreResult = Low;
        }
        else
        {
            this.ScoreResult = High;
        }
    }

    /// <summary>
    /// Use this with if you haven't initialize the object
    /// </summary>
    /// <returns>Based On two taken scores at at given time return if the score is low or high</returns>
    public static string CalculateScoreResult(int score1, int score2, float timeForScore1, float timeForScore2)
    {
        var result = (score2 - score1) / DividerTwo * (timeForScore2 - timeForScore1);
        
        if (result < ControlScoreForScoreResult)
        {
            return Low;
        }
        else
        {
            return High;
        }
    }

    /// <summary>
    /// Use this with initialized objects
    /// </summary>
    public void CalculateTimeResult()
    {
        if (this.TimeToCompare > this.CurrentTime)
        {
            this.TimeResult = Low;
        }
        else
        {
            this.TimeResult = High;
        }
    }

    /// <summary>
    /// Use this with if you haven't initialize the object
    /// </summary>
    public static string CalculateTimeResult(float currentTime, float timeToCompare)
    {
        if (timeToCompare < currentTime)
        {
            return High;
        }
        else
        {
            return Low;
        }
    }

    /// <summary>
    /// Use this with initialized objects
    /// </summary>
    public double CalculateEfficiency()
    {
        var result = this.CountHits / this.CountFiredShots;

        return result;
    }

    /// <summary>
    /// Use this with if you haven't initialize the object
    /// </summary>
    public static double CalculateEfficiency(double countHits, double countFiredShots)
    {
        var result = countHits / countFiredShots;

        return result;
    }

    #endregion Methods
}
