﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using System.Globalization;

public class GameManager : MonoBehaviour
{

    // make game manager public static so can access this from other scripts
    public static GameManager gm;

    // public variables
    public int score = 0;

    public bool canBeatLevel = false;
    public int beatLevelScore = 20;

    public float startTime = 5.0f; //set in the game 

    public Text mainScoreDisplay;
    public Text mainTimerDisplay;

    public GameObject gameOverScoreOutline;

    public AudioSource musicAudioSource;

    public bool gameIsOver = false;

    public GameObject playAgainButtons;
    public string playAgainLevelToLoad;

    public GameObject nextLevelButtons;
    public string nextLevelToLoad;

    private float currentTime;

    // reference to log file
    private LoggerCSV logger;
    //private float lastLoggingTime;

    // reference to Dificulty Changer helper class
    private DificultyChanger difChanger;

    // starting from 0 counts what time did he spent on each level. Needed because for changing difficulty cant use the currentTime because it can go up.
    private float inGameTimer;

    // with this i will track when a in game second has passed so i can track the score result
    private float trackTimer;

    // Using this to check if the tracked time has passed this many seconds
    private float OnSecondsToTrack = 4f;

    private SpawnGameObjects spawnGameObjects;

    private ParticleSystem rain;
    private ParticleSystem.EmissionModule rainEmission;
    private ParticleSystem.MinMaxCurve rainMinMaxCurve;

    // for sending log files over server
    //[SerializeField] private int portNum;

    //[SerializeField] private string hostName;

    //SocketClient client = new SocketClient();
    //bool endOfCalibrationPeriod = false;
    //bool hasSocketConnection = false;

    // setup the game
    void Start()
    {
        //try
        //{
        //    client = new SocketClient();
        //    client.StartSocketClient(portNum,hostName);
        //    hasSocketConnection = true;
        //}
        //catch (SocketException ex)
        //{
        //    hasSocketConnection = false;
        //    Debug.Log("The socket connection can't be established.");
        //}

        //string socp = (string)client.SendMessageToSocketServer("SOCP");

        // set the current time to the startTime specified
        currentTime = startTime;

        // set in game timer and track timer to 0
        inGameTimer = 0f;
        trackTimer = 0f;
        //lastLoggingTime = startTime;

        // get a reference to the GameManager component for use by other scripts
        if (gm == null)
            gm = this.gameObject.GetComponent<GameManager>();

        // init scoreboard to 0
        mainScoreDisplay.text = "0";

        // inactivate the gameOverScoreOutline gameObject, if it is set
        if (gameOverScoreOutline)
            gameOverScoreOutline.SetActive(false);

        // inactivate the playAgainButtons gameObject, if it is set
        if (playAgainButtons)
            playAgainButtons.SetActive(false);

        // inactivate the nextLevelButtons gameObject, if it is set
        if (nextLevelButtons)
            nextLevelButtons.SetActive(false);

        logger = GameObject.Find("LoggerCSV").GetComponent<LoggerCSV>();
        if (logger == null)
        {
            Debug.Log("Unable to set reference to LoggerCSV.");
        }
        else
        {
            logger.Log("New game for user with email: " + PlayerPrefs.GetString("email"));
        }

        difChanger = new DificultyChanger()
        {
            CurrentTime = currentTime,
            TimeToCompare = currentTime
        };
        if (difChanger == null)
        {
            Debug.Log("Unable to set reference to DificultyChanger.");
        }


        spawnGameObjects = GameObject.FindObjectOfType<SpawnGameObjects>();
        if (spawnGameObjects == null)
        {
            Debug.Log("Unable to set reference to Spawner.");
        }

        // Resetting the magnitude of objects after every level
        foreach (var item in spawnGameObjects.spawnObjects)
        {
            TargetMover target = item.GetComponent<TargetMover>();
            
            target.motionMagnitude = 0.2f;
        }

        // Resetting fog and rain for each level
        switch (playAgainLevelToLoad)
        {
            case "Level2":
                RenderSettings.fogDensity = 0.08f;
                break;
            case "Level3":
                RenderSettings.fogDensity = 0.02f;

                // initialize rain
                rain = GameObject.Find("Rain").GetComponent<ParticleSystem>();
                
                if (rain == null)
                {
                    Debug.Log("Unable to set reference to Rain.");
                }

                rainEmission = rain.emission;

                rainMinMaxCurve = rainEmission.rateOverTime;
                rainMinMaxCurve.mode = ParticleSystemCurveMode.Constant;
                rainMinMaxCurve.constant = 85000f;
                break;
            case "Level4":
                RenderSettings.fogDensity = 0.1f;

                // initialize rain
                rain = GameObject.Find("Rain").GetComponent<ParticleSystem>();

                if (rain == null)
                {
                    Debug.Log("Unable to set reference to Rain.");
                }
                rainEmission = rain.emission;

                rainMinMaxCurve = rainEmission.rateOverTime;
                rainMinMaxCurve.mode = ParticleSystemCurveMode.Constant;
                rainMinMaxCurve.constant = 85000f;
                break;
            default:
                RenderSettings.fog = false;
                break;
        }
    }

    // this is the main game event loop
    void Update()
    {
        if (!gameIsOver)
        {
            if (canBeatLevel && (score >= beatLevelScore))
            {
                // check to see if beat game
                BeatLevel();
            }
            else if (currentTime < 0)
            {
                // check to see if timer has run out
                EndGame();
            }
            else
            {
                // game playing state, so update the timer and in game timer
                currentTime -= Time.deltaTime;
                inGameTimer += Time.deltaTime;

                difChanger.CurrentTime = currentTime;
                //Debug.Log(inGameTimer);
                mainTimerDisplay.text = currentTime.ToString("0.00");
                
                // on update to log the results of player
                LogPlayerResult();

                // if 4 second has past from last time we have tracked time track it again
                if (inGameTimer - trackTimer >= OnSecondsToTrack)
                {
                    // getting result for score
                    trackTimer = inGameTimer;
                    TrackScoreResults();

                    // getting result for time
                    TrackTimeResult();

                    // Changing difficulty based on which level we are
                    ChangeDifficult(playAgainLevelToLoad);
                }
            }
        }
    }

    void EndGame()
    {
        // game is over
        gameIsOver = true;

        // re-purpose the timer to display a message to the player
        mainTimerDisplay.text = "GAME OVER";

        // activate the gameOverScoreOutline gameObject, if it is set 
        if (gameOverScoreOutline)
            gameOverScoreOutline.SetActive(true);

        // activate the playAgainButtons gameObject, if it is set 
        if (playAgainButtons)
            playAgainButtons.SetActive(true);

        // reduce the pitch of the background music, if it is set 
        if (musicAudioSource)
            musicAudioSource.pitch = 0.5f; // slow down the music
    }

    void BeatLevel()
    {
        // game is over
        gameIsOver = true;

        // repurpose the timer to display a message to the player
        mainTimerDisplay.text = "LEVEL COMPLETE";

        // activate the gameOverScoreOutline gameObject, if it is set 
        if (gameOverScoreOutline)
            gameOverScoreOutline.SetActive(true);

        // activate the nextLevelButtons gameObject, if it is set 
        if (nextLevelButtons)
            nextLevelButtons.SetActive(true);

        // reduce the pitch of the background music, if it is set 
        if (musicAudioSource)
            musicAudioSource.pitch = 0.5f; // slow down the music
    }

    // public function that can be called to update the score or time
    public void targetHit(int scoreAmount, float timeAmount)
    {
        // increase the score by the scoreAmount and update the text UI
        score += scoreAmount;
        mainScoreDisplay.text = score.ToString();

        // increase the time by the timeAmount
        currentTime += timeAmount;

        // don't let it go negative
        if (currentTime < 0)
            currentTime = 0.0f;

        // update the text UI
        mainTimerDisplay.text = currentTime.ToString("0:00");

        //this is if we want every time he hits a target to log it
        //LogPlayerResult();
    }

    // public function that can be called to restart the game
    public void RestartGame()
    {
        // we are just loading a scene (or reloading this scene)
        // which is an easy way to restart the level
        //Application.LoadLevel (playAgainLevelToLoad);
        SceneManager.LoadScene(playAgainLevelToLoad);
    }

    // public function that can be called to go to the next level of the game
    public void NextLevel()
    {
        // we are just loading the specified next level (scene)
        //Application.LoadLevel (nextLevelToLoad);
        SceneManager.LoadScene(nextLevelToLoad);
    }

    private void LogPlayerResult()
    {
        // IMPORTANT WARNING 
        /*
         * If the culture of your computer doesn't accept ',' as delimiter comment bellow code 
         */
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        var formattedCurrentTime = currentTime.ToString("F3", nfi);

        // IMPORTANT WARNING 
        logger.Log(String.Format("{0}; {1}; {2};", score, formattedCurrentTime, playAgainLevelToLoad));
    }

    // this should be called every 4 in game second has past
    private void TrackScoreResults()
    {
        difChanger.Score2 = score;
        difChanger.TimeForScore2 = inGameTimer;
        
        difChanger.CalculateScoreResult();

        difChanger.Score1 = difChanger.Score2;
        difChanger.TimeForScore1 = difChanger.TimeForScore2;
    }

    private void TrackTimeResult()
    {
        difChanger.CalculateTimeResult();

        difChanger.TimeToCompare = currentTime;
    }

    private void ChangeDifficult(string level) //TODO you can do it without the param level use playAgainLevelToLoad
    {
        SetModifiers();

        switch (level)
        {
            // Changing how fast the objects are moving(speed)
            case "Level1":
                ChangeSpeed(difChanger.SpeedModifier);

                break;
            // Changing speed and density of fog(densityFog)
            case "Level2":
                ChangeSpeed(difChanger.SpeedModifier);
                ChangeDensityFog(difChanger.FogModifier);

                break;
            // Changing speed and density of rain(densityRain)
            case "Level3":
                ChangeSpeed(difChanger.SpeedModifier);
                ChangeDensityFog(difChanger.FogModifier);
                ChangeDensityRain(difChanger.RainModifier);
                break;
            // Changing speed, densityFog and densityRain
            case "Level4":
                ChangeSpeed(difChanger.SpeedModifier);
                ChangeDensityFog(difChanger.FogModifier);
                ChangeDensityRain(difChanger.RainModifier);
                break;
            default:
                break;
        }

        ResetModifiers();
    }

    private void ResetModifiers()
    {
        difChanger.SpeedModifier = 0f;
        difChanger.FogModifier = 0f;
        difChanger.RainModifier = 0f;
    }

    private void SetModifiers()
    {
        if (difChanger.ScoreResult == "low" && difChanger.TimeResult == "low")
        {
            difChanger.SpeedModifier = -0.04f;
            difChanger.FogModifier = -0.02f;
            difChanger.RainModifier = -10000f;
        }

        if (difChanger.ScoreResult == "high" && difChanger.TimeResult == "low")
        {
            difChanger.SpeedModifier = -0.02f;
            difChanger.FogModifier = -0.01f;
            difChanger.RainModifier = -5000f;
        }

        if (difChanger.ScoreResult == "low" && difChanger.TimeResult == "high")
        {
            difChanger.SpeedModifier = 0.02f;
            difChanger.FogModifier = 0.01f;
            difChanger.RainModifier = 5000f;
        }

        if (difChanger.ScoreResult == "high" && difChanger.TimeResult == "high")
        {
            difChanger.SpeedModifier = 0.04f;
            difChanger.FogModifier = 0.02f;
            difChanger.RainModifier = 10000f;
        }
    }

    private void ChangeSpeed(float modifier)
    {
        float modifiedSpeed = spawnGameObjects.spawnObjects[0].GetComponent<TargetMover>().motionMagnitude + (modifier);

        if (modifiedSpeed <= 0.50f && modifiedSpeed >= 0.01f)
        {
            foreach (var item in spawnGameObjects.spawnObjects)
            {
                TargetMover target = item.GetComponent<TargetMover>();
                
                target.motionMagnitude = target.motionMagnitude + modifier;
            }
        }
    }

    private void ChangeDensityFog(float modifier)
    {
        float modifiedDensityFog = RenderSettings.fogDensity + (modifier);

        if (modifiedDensityFog <= 0.2f && modifiedDensityFog >= 0.01f)
        {
            RenderSettings.fogDensity += modifier;
        }
    }

    private void ChangeDensityRain(float modifier)
    {
        var rateOverTime = rainMinMaxCurve.constant + (modifier);

        if (rateOverTime <= 100000f && rateOverTime >= 30000f)
        {
            rainMinMaxCurve.constant += modifier;

            rainEmission.rateOverTime = rainMinMaxCurve;
        }
    }

}
