﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{

    public InputField email;
    public Text validEmail;

    public void LoadMainScene()
    {
        string emailMatchingExpr = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                 + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                 + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                 + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
        PlayerPrefs.SetString("email", email.text);
        if (!string.IsNullOrEmpty(email.text) && Regex.IsMatch(email.text, emailMatchingExpr))
        {
            SceneManager.LoadScene("Level1");
        }
        else
        {
            validEmail.gameObject.SetActive(true);
        }
    }
}
