<?php 
    $isFileExists = false; 
    if(file_exists($_GET['filename'])) 
    { 
        $isFileExists = true; 
    } 
    
    $logFile = fopen($_GET['filename'], "a") or die("Unable to open file!"); 
    if(!$isFileExists) 
    { 
        $txt = $_GET['columnNames']; 
        fwrite($logFile, $txt); 
        fwrite($logFile, "\n"); 
    } 
    
    $txt = $_GET['logContent']; 
    fwrite($logFile, $txt); 
    fwrite($logFile, "\n"); 

    fclose($logFile); 
    http_response_code(200); 
?>